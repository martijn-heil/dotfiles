# Here I keep some of my personal dotfiles

## Cloning dotfiles to new user
```
cd "$HOME"
echo "alias dotfiles='git --git-dir=\"\$HOME/.dotfiles\" --work-tree=\"\$HOME\"'" >> ~/.bashrc
echo '.dotfiles' >> .gitignore
git clone --bare 'https://gitlab.com/martijn-heil/dotfiles.git' "$HOME/.dotfiles"
dotfiles checkout
```

Will probably error with a message like:
```
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .screenrc
Please move or remove them before you can switch branches.
Aborting
```

Back up and remove all listed files, then checkout again:
```
dotfiles checkout
```

Then make sure unrelated files are ignored:
```
dotfiles config --local status.showUntrackedFiles no
```
