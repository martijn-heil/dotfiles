#
# ~/.bashrc
#

HISTSIZE=5000
HISTFILESIZE=5000
HISTCONTROL=ignorespace

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

cd "$HOME"

PROMPT_COMMAND='history -a; history -r'

alias rmt='rmtrash'
alias ls='ls --color=auto'
alias dotfiles='git --git-dir="$HOME/.dotfiles/" --work-tree="$HOME"'
alias sc='systemctl'


PS1='[\u@\h \W]\$ '
export PATH=$PATH:~/bin:~/.cargo/bin:/opt/npm-global/bin

export BAGIMPORT_RD="+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs"
export EDITOR=vim

alias urldecode='python3 -c "import sys, urllib.parse as ul; print(ul.unquote_plus(sys.argv[1]))"'
alias urlencode='python3 -c "import sys, urllib.parse as ul; print(ul.quote_plus(sys.argv[1]))"'
alias journalctl='journalctl -o with-unit'


# The first word of each simple command, if unquoted, is checked to see if it has an alias.
# ...
# If the last character of the alias value is a space or a tab character,
# then the next command word following the alias is also checked for alias expansion.\
#
# This allows you to use `sudo sc start nginx` for example.
alias sudo='sudo '

# Use vi input mode
set -o vi

[ -f "$HOME/.bashrc.local" ] && source "$HOME/.bashrc.local"
[ -f '/usr/share/fzf/key-bindings.bash' ] && source '/usr/share/fzf/key-bindings.bash'
